# Filez

Yet another file server

## Getting started

1. Clone the repository
   ```
   git clone git@gitlab.com:joao-m-santos/filez.git
   ```
2. Install dependencies
   ```
   $ npm install
   ```
3. Run server
   ```
   $ npm start
   ```
