const fs = require("fs");
const path = require("path");
const express = require("express");
const multer = require("multer");
const cors = require("cors");
const { nanoid } = require("nanoid");

const FILE_PATH = path.join(process.cwd(), "public/files");

if (!fs.existsSync(FILE_PATH)) {
  fs.mkdirSync(FILE_PATH);
}

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

const app = express();
const port = 4000;

app.use(cors());

app.post("/upload", upload.single("file"), (req, res) => {
  const id = nanoid(8);
  const filename = id + "." + getExt(req.file.originalname);

  const stream = fs.createWriteStream(path.join(FILE_PATH, filename));

  stream.write(req.file.buffer, () => {
    res.send(filename);
  });

  console.log(`${Date.now()} :: new file uploaded [${filename}]`);
});

app.use(express.static("public"));

const server = app.listen(port, () => {
  console.log(`Filez running on port ${port}`);
});

process.on("exit", cleanup);
process.on("SIGINT", cleanup);
process.on("SIGQUIT", cleanup);
process.on("SIGTERM", cleanup);

function getExt(filename) {
  return filename.split(".").pop();
}

function removeAllFiles(directory) {
  fs.readdir(directory, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      fs.unlink(path.join(directory, file), (err) => {
        if (err) throw err;
      });
    }
  });
}

function cleanup() {
  server.close();
  console.log("File cleanup started.");
  removeAllFiles(FILE_PATH);
  console.log("File cleanup ended.");
}
